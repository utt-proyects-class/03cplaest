from django.urls import path

from home import views

app_name = "home"

urlpatterns = [
    path('', views.Index.as_view(), name="index"),
    path('create/category/', views.CreateCategory.as_view(), name="create_category"),
    path('detail/category/<int:pk>/', views.DetailCategory.as_view(), name="detail_category"),
    path('update/category/<int:pk>/', views.UpdateCategory.as_view(), name="update_category"),
    path('delete/category/<int:pk>/', views.DeleteCategory.as_view(), name="delete_category"),
    ### USERS URLS
    path('list/users/', views.UserList.as_view(), name="list_users"),
]