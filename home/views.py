from django.shortcuts import render
from django.views import generic
from django.urls import reverse_lazy

from django.contrib.auth.models import User

from .models import Category
from .forms import UpdateCategoryForm, CreateCategoryForm
# Create your views here.


##### C R U D  CATEGORY #####

#CREATE
class CreateCategory(generic.CreateView):
    template_name = "home/create_category.html"
    model = Category
    form_class = CreateCategoryForm
    success_url = reverse_lazy("home:index")


#RETRIEVE
## List Categories
class Index(generic.View):
    template_name = "home/index.html"
    context = {}

    def get(self, request, *args, **kwargs):
        self.context = {
            "categories": Category.objects.all()
        }
        return render(request, self.template_name, self.context)
    

## Detail Category
class DetailCategory(generic.View):
    template_name = "home/detail_category.html"
    context = {}

    def get(self, request, pk):
        self.context = {
            "category": Category.objects.get(pk=pk)
        }
        return render(request, self.template_name, self.context)



# UPDATE 
class UpdateCategory(generic.UpdateView):
    template_name = "home/update_category.html"
    model = Category
    form_class = UpdateCategoryForm
    success_url = reverse_lazy("home:index")


# DELETE 
class DeleteCategory(generic.DeleteView):
    template_name = "home/delete_category.html"
    model = Category
    success_url = reverse_lazy("home:index")


##### U S E R S  C R U D  #####

## RETRIEVE
# User List 
class UserList(generic.View):
    template_name = "home/list_users.html"
    context = {}

    def get(self, request):
        self.context = {
            "users": User.objects.all()
        }
        return render(request, self.template_name, self.context)