from django.contrib import admin

# Register your models here.
from .models import Profile, Category, Product

@admin.register(Profile)
class ProfileAdmin(admin.ModelAdmin):
    list_display = [
        "user",
        "name",
        "timestamp",
        "status"
    ]


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = [
        "name",
        "user",
        "id",
        "status"
    ]


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = [
        "product_name",
        "category",
        "status",
        "id"
    ]